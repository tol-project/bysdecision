//////////////////////////////////////////////////////////////////////////////
// FILE   : linear.tol
// PURPOSE: Bayesian decision methods which cost function is separable for 
//          each component x in this way
//           
//  F(D,X) = Sum { f[i](d[i],x[i]), i = 1 ... n }
// 
// where each f[i] has this generic form
// 
//             /
//            |  a0+(x-d)*a1  if x >= d
//   f(d,x) = <
//            |  b0+(d-x)*b1  if x <  d
//             \
// 
// This function meassures the cost of taking the decision d when random
// variable takes the value x with 
// 
// Optionally it's posible to add linear constraining equations and or 
// inequations
// 
//  H * x = h 
// 
//  G * x <= g 
// 
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
Class @Linear.Constraininig.Equation : @GenericDecision
//////////////////////////////////////////////////////////////////////////////
{
  //Linear equations
  Matrix _.H;
  Matrix _.h;

  Real constraining.equation(Matrix d, Matrix grad)
  {
    Matrix r = _.H*d-_.h;
    Real R = MatSum(r^2);
    Real If(Rows(grad),
    {
      Matrix grad := Tra(Tra(r)*_.H)*2;
      True   
    });
    R
  };

  Real setup.Linear.Constraininig.Equation(Real void)
  {
    //Has constraining equations
    Real _.has.constraining.equation := 1;
    True
  }

};

//////////////////////////////////////////////////////////////////////////////
Class @Linear.Constraininig.Inequation : @GenericDecision
//////////////////////////////////////////////////////////////////////////////
{
  //Linear inequations
  Matrix _.G;
  Matrix _.g;

  Real constraining.inequation(Matrix d, Matrix grad)
  {
    MatMax(_.G*d-_.g)
  };

  Real setup.Linear.Constraininig.Inequation(Real void)
  {
    //Has constraining equations
    Real _.has.constraining.inequation := 1;
    True
  }
};


//////////////////////////////////////////////////////////////////////////////
//Abstract class for decision with linear cost function
Class @Linear.Cost : @MappingDecision
//////////////////////////////////////////////////////////////////////////////
{
  Real setup.Linear.Cost(Real void)
  {
    setup.GenericDecision(void);
    setup.MappingDecision(void);
    Real _.id_useGradient := True;
    True
  };
  Matrix _cost_from_error(Matrix E, Matrix A, Matrix B, Matrix deriv);
  Real cost.average(Matrix d, Matrix grad)
  {
  //WriteLn("TRACE @Linear.Cost::cost.average 1");
    Matrix D = Group("ConcatRows",NCopy(_.m,Matrix Tra(d)));
  //WriteLn("TRACE @Linear.Cost::cost.average 2");
    Matrix A = GE(_.X,D);
  //WriteLn("TRACE @Linear.Cost::cost.average 3");
    Matrix B = Not(A);
  //WriteLn("TRACE @Linear.Cost::cost.average 4");
    Matrix E = _.X-D;
  //WriteLn("TRACE @Linear.Cost::cost.average 5");
    Matrix deriv = If(Rows(grad),Constant(_.m,_.n,?),Constant(0,0,?));
  //WriteLn("TRACE @Linear.Cost::cost.average 6");
    Matrix C = _cost_from_error(E,A,B,deriv);
  //WriteLn("TRACE @Linear.Cost::cost.average 7");
    Real If(Rows(grad),
    {
    //WriteLn("TRACE @Linear.Cost::cost.average 8.1 _.m="<<_.m+" _.n="<<_.n);
      Matrix grad := -Tra(Constant(1,_.m,1/_.m)*deriv);
    //WriteLn("TRACE @Linear.Cost::cost.average 8.2 grad:("<<Rows(grad)+","<<Columns(grad)+")");
    //WriteLn("TRACE @Linear.Cost::cost.average 8.3 grad=\n"<<grad+"\n");
      True      
    });
  //WriteLn("TRACE @Linear.Cost::cost.average 9");
    MatSum(C) * 1/_.m
  }
};

//////////////////////////////////////////////////////////////////////////////
//Decision with symmetrical linear cost function 
// 
// c(d,x) = |d-x|
//
Class @Linear.Cost.Sym : @Linear.Cost
//////////////////////////////////////////////////////////////////////////////
{
  Real cost(Matrix d, Matrix x)
  {
    MatSum(Abs(d-x))
  };
  Matrix _cost_from_error(Matrix E, Matrix A, Matrix B, Matrix deriv)
  {
    If(Rows(deriv),
    {
      Matrix U = E*0+1;
      deriv := (U $* A) - (U $* B)
    });
    (E $* A) - (E $* B)
  }
};
  
//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.Sym.Unconstrained : 
  @NonConstrainingEquations,
  @NonConstrainingInequations,
  @Linear.Cost.Sym
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.Sym.LinEqu : 
  @Linear.Constraininig.Equation,
  @NonConstrainingInequations,
  @Linear.Cost.Sym
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost(void);
    setup.Linear.Constraininig.Equation(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.Sym.LinInq : 
  @NonConstrainingEquations,
  @Linear.Constraininig.Inequation,
  @Linear.Cost.Sym
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost(void);
    setup.Linear.Constraininig.Inequation(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.Sym.LinEquInq : 
  @Linear.Constraininig.Equation, 
  @Linear.Constraininig.Inequation,
  @Linear.Cost.Sym
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost(void);
    setup.Linear.Constraininig.Equation(void);
    setup.Linear.Constraininig.Inequation(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
//Decision with asymmetrical continuous linear cost function
//
//            /
//           | a*(x-d) si x>=d
// c(d,x) = <
//           | b*(d-x) si x<=d
//            \
//
Class @Linear.Cost.AsymCont : @Linear.Cost
//////////////////////////////////////////////////////////////////////////////
{
  //Cost coefficients
  Matrix _.a1;
  Matrix _.b1;
  //Expanded cost coefficients
  Matrix _A1 = Constant(0,0,?);
  Matrix _B1 = Constant(0,0,?);
  Real setup.Linear.Cost.AsymCont(Real void)
  {
    setup.Linear.Cost(void);
    Matrix _A1 := Group("ConcatRows",NCopy(_.m,Matrix Tra(_.a1)));
    Matrix _B1 := Group("ConcatRows",NCopy(_.m,Matrix Tra(_.b1)));
    True
  };
  Real setup(Real void)
  {
    setup.Linear.Cost.AsymCont(void)
  };
  Real cost(Matrix d, Matrix x)
  {
    MatSum(IfMat(LE(d,x),_.a1 $* (x-d), _.b1 $* (d-x)))
  };
  Matrix _cost_from_error(Matrix E, Matrix A, Matrix B, Matrix deriv)
  {
    If(Rows(deriv),
    {
      Matrix U = E*0+1;
      deriv := ((_A1 $* U ) $* A) - ((_B1 $* U ) $* B)
    });
    ((_A1 $* E ) $* A) - ((_B1 $* E ) $* B)
  }
};
  
//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.AsymCont.Unconstrained : 
  @NonConstrainingEquations,
  @NonConstrainingInequations,
  @Linear.Cost.AsymCont
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost.AsymCont(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.AsymCont.LinEqu : 
  @Linear.Constraininig.Equation,
  @NonConstrainingInequations,
  @Linear.Cost.AsymCont
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost.AsymCont(void);
    setup.Linear.Constraininig.Equation(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.AsymCont.LinInq : 
  @NonConstrainingEquations,
  @Linear.Constraininig.Inequation,
  @Linear.Cost.AsymCont
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost.AsymCont(void);
    setup.Linear.Constraininig.Inequation(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.AsymCont.LinEquInq : 
  @Linear.Constraininig.Equation, 
  @Linear.Constraininig.Inequation,
  @Linear.Cost.AsymCont
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost.AsymCont(void);
    setup.Linear.Constraininig.Equation(void);
    setup.Linear.Constraininig.Inequation(void)
  }
};



//////////////////////////////////////////////////////////////////////////////
//Decision with asymmetrical discontinuous linear cost function
// 
//            /
//           | a0+a1*(x-d) si x>=d
// c(d,x) = <
//           | b0+b1*(d-x) si x<=d
//            \
// 
Class @Linear.Cost.AsymDisc : @Linear.Cost
//////////////////////////////////////////////////////////////////////////////
{
  //Cost coefficients
  Matrix _.a0;
  Matrix _.a1;
  Matrix _.b0;
  Matrix _.b1;
  //Expanded cost coefficients
  Matrix _A0 = Constant(0,0,?);
  Matrix _A1 = Constant(0,0,?);
  Matrix _B0 = Constant(0,0,?);
  Matrix _B1 = Constant(0,0,?);
  Real setup.Linear.Cost.AsymDisc(Real void)
  {
    setup.Linear.Cost(void);
    Matrix _A0 := Group("ConcatRows",NCopy(_.m,Matrix Tra(_.a0)));
    Matrix _A1 := Group("ConcatRows",NCopy(_.m,Matrix Tra(_.a1)));
    Matrix _B0 := Group("ConcatRows",NCopy(_.m,Matrix Tra(_.b0)));
    Matrix _B1 := Group("ConcatRows",NCopy(_.m,Matrix Tra(_.b1)));
    True
  };
  Real cost(Matrix d, Matrix x)
  {
    MatSum(IfMat(LE(d,x),_.a0+_.a1 $* (x-d),_.b0+_.b1 $* (d-x)))
  };
  Matrix _cost_from_error(Matrix E, Matrix A, Matrix B, Matrix deriv)
  {
    If(Rows(deriv),
    {
      Matrix U = E*0+1;
      deriv := ((_A1 $* U ) $* A) - ((_B1 $* U ) $* B)
    });
    ((_A0 + _A1 $* E ) $* A) + ((_B0 - _B1 $* E ) $* B)
  }
};
  
//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.AsymDisc.Unconstrained : 
  @NonConstrainingEquations,
  @NonConstrainingInequations,
  @Linear.Cost.AsymDisc
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost.AsymDisc(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.AsymDisc.LinEqu : 
  @Linear.Constraininig.Equation,
  @NonConstrainingInequations,
  @Linear.Cost.AsymDisc
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost.AsymDisc(void);
    setup.Linear.Constraininig.Equation(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.AsymDisc.LinInq : 
  @NonConstrainingEquations,
  @Linear.Constraininig.Inequation,
  @Linear.Cost.AsymDisc
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost.AsymDisc(void);
    setup.Linear.Constraininig.Inequation(void)
  }
};

//////////////////////////////////////////////////////////////////////////////
Class @Linear.Cost.AsymDisc.LinEquInq : 
  @Linear.Constraininig.Equation, 
  @Linear.Constraininig.Inequation,
  @Linear.Cost.AsymDisc
//////////////////////////////////////////////////////////////////////////////
{
  Real setup(Real void)
  {
    setup.Linear.Cost.AsymDisc(void);
    setup.Linear.Constraininig.Equation(void);
    setup.Linear.Constraininig.Inequation(void)
  }
};





